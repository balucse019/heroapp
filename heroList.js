(function(angular) {
  'use strict';
function HeroListController($scope, $element, $attrs) {
  var ctrl = this;

  // This would be loaded by $http etc.
  ctrl.list = [
    {
      name: 'Wonder Woman',
      company: 'DC',
      powers:[]
    },
    {
      name: 'Superman',
      company: '',
      powers:[]
    },
    {
      name: 'Batman',
      company: 'DC',
      powers:[]
    },
    {
      name: 'Spider-Man',
      company: 'Marvel',
      powers:[]
    },
    {
      name: 'Wolverine',
      company: 'Marvel',
      powers:[]
    }
    
  ];
  
  ctrl.companyList = ['Marvel','DC'];
  
  ctrl.powers = ['Hammer','Warrior','spider-sense','acrobatic leaps',
'web-slinging'];

  ctrl.updateHero = function(hero, prop, value) {
    hero[prop] = value;
  };

  ctrl.deleteHero = function(hero) {
    var idx = ctrl.list.indexOf(hero);
    if (idx >= 0) {
      ctrl.list.splice(idx, 1);
    }
  };
}

angular.module('heroApp').component('heroList', {
  templateUrl: 'heroList.html',
  controller: HeroListController
});
})(window.angular);

/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/